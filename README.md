# PROJECT-NAME

## Create the virtual environment

To create the dev virtual environment:

```bash
./create-venv 1
```

To create the virtual environment:

```bash
./create-venv
```

## Start

```bash
./start
```
