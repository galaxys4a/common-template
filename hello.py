"""
"Hello, World!".

Functions:

    say_hello() -> None
"""


def say_hello() -> None:
    print("Hello, World!")
